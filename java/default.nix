with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "gradle-projects";

  buildInputs = [
    gradle
  ];
}
