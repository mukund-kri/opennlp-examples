package learn.opennlp;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * The Main runner for the OpenNLP basic example
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.14
 */
class Main {


    @Argument(required=true)
    private String exampleName;

    public static void main(String[] args) {

        try {
            new Main().runMain(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse the command-line arguments and run the appropriate example class
     *
     * @param args String[] the args that is passed the class main
     *
     * @return void
     */
    public void runMain(String[] args) throws Exception {

        CmdLineParser parser = new CmdLineParser(this);
        parser.parseArgument(args);

        System.out.println("Running Example :: " + this.exampleName + "\n");

        switch(this.exampleName.toLowerCase()) {

        case "detect-language":
            LanguageDetectorExample lde = new LanguageDetectorExample();
            lde.detectSentencesInFile("multi.language.sentences.txt");
            break;

        case "detect-sentence":
            // Show how to split apart sentences
            SentenceDetectorExample sde = new SentenceDetectorExample();
            sde.detect("First sentence. Second sentence");
            break;

        case "tokenize":
            // A simple sentence tokenizer example
            TokenizerExample te = new TokenizerExample();
            te.runExample("An input sample sentence.");
            break;

        case "ner":
            // Named entity recognition. In this case, I do only people and
            // organizations
            NERExample1 ner1 = new NERExample1();
            ner1.runExample("ner.input.txt"); 
            break;

        case "pos":
            // Parts of Speech tagging
            POSTaggerExample posTaggerExample = new POSTaggerExample();
            posTaggerExample.runExample("ner.input.txt");
            break;

        case "lemma":
            // Lemmatization example
            LemmatizerExample lemmaExample = new LemmatizerExample();
            lemmaExample.runExample("ner.input.txt");
            break;
            
        default:
            System.out.println("Unknown Example !!!!");
        }
    }
}
