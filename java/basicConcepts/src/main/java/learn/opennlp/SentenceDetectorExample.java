package learn.opennlp;

import java.io.FileInputStream; 
import java.io.InputStream;  

import opennlp.tools.sentdetect.SentenceDetectorME; 
import opennlp.tools.sentdetect.SentenceModel;


/**
 * Demonstrates how to use OpenNLP's sentence detector with the pre-trained
 * model.
 *
 * @author  Mukund Krishnamurthy
 * @version 0.0.1
 * @since   2018.06.09
 */
class SentenceDetectorExample {

    SentenceDetectorME sentenceDetector;

    /**
     * Read in and load the pre-trained English sentence detector model.
     */
    SentenceDetectorExample() {
        try (InputStream modelIn = new FileInputStream("en-sent.bin")) {
            SentenceModel model = new SentenceModel(modelIn);
            this.sentenceDetector = new SentenceDetectorME(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Detect, split and print each sentence in a given string.
     *
     * @param  str The input raw String that gets passed to OpenNLP's sentence
     *              detector
     * @return void
     */
    public void detect(String str) {
        //Detecting the sentence 
        String sentences[] = this.sentenceDetector.sentDetect(str); 
    
        //Printing the sentences 
        for(String sent : sentences)        
            System.out.println(sent); 
    }
}
