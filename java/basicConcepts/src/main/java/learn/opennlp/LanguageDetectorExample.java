package learn.opennlp;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import opennlp.tools.langdetect.LanguageDetectorModel;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.Language;

/**
 * Demonstrates how to use OpenNLP's language detection. This version uses the
 * pre-trained model provided by Apache OpenNLP team.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.14
 */
class LanguageDetectorExample {

    LanguageDetectorME langDetector;

    /**
     * Read in and load the pre-trained language detector model.
     */
    LanguageDetectorExample() {
        try(InputStream modelIn = new FileInputStream("langdetect-183.bin")) {
            LanguageDetectorModel model = new LanguageDetectorModel(modelIn);
            this.langDetector = new LanguageDetectorME(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Read a file with many sentences and print to screen predicted languages
     * of each sentence
     *
     * @param fileName Sentence text file name as String.
     *
     * @return void
     */
    public void detectSentencesInFile(String fileName) {
        //read file into stream, try-with-resources

       
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach((sentence) -> {
                    Language lang = langDetector.predictLanguage(sentence);
                    System.out.println("Best language: " + lang.getLang());
                    System.out.println("Best language confidence: " + lang.getConfidence());
                });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
