package learn.opennlp;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;


/**
 * Code showing simple lemmatization code using opennlp
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.19
 */
class LemmatizerExample {

    DictionaryLemmatizer lemmatizer;
    POSTaggerExample tagger = new POSTaggerExample();

    /**
     * Load the pre-trained, maximum entropy lemmatizer model. 
     */
    LemmatizerExample() {
        try(InputStream modelFile = new FileInputStream("en-lemmatizer.dict.txt")) {
            lemmatizer = new DictionaryLemmatizer(modelFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run Lemmatization on a set of sentences loaded from an text file. 
     *
     * @param fileName Name of the file which has the set of sentences that
     *                 have to be lemmatized
     *
     * @return void
     */
    public void runExample(String fileName) {
        System.out.println("Running Lemmatizer example ....");

        try(Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach((sentence) -> {
                    System.out.println(sentence);
                    String[][] tokensNTags = tagger.tokensAndTags(sentence);
                    String[] lemmas = lemmatizer.lemmatize(tokensNTags[0], tokensNTags[1]);

                    for(String lemma : lemmas) {
                        System.out.println(lemma);
                    }
                    System.out.println("\n");
                });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
