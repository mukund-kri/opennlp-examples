package learn.opennlp;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;

/**
 * Example code for Named Entity Detection, using OpenNLP. This particular
 * version only does - People and Organizations for the English language.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.15
 */
class NERExample1 {

    NameFinderME[] nameFinders;
    String[] modelFiles = {"en-ner-person.bin", "en-ner-organization.bin"};
    TokenizerExample te = new TokenizerExample();

    /**
     * Load up the pre-trained models of people and organization recogintion.
     */
    NERExample1() {

        this.nameFinders = new NameFinderME[2];
        for(int i=0; i<2; i++) {
            String modelFile = this.modelFiles[i];
            try(InputStream modelFileStream = new FileInputStream(modelFile)) {
                TokenNameFinderModel model = new TokenNameFinderModel(modelFileStream);
                nameFinders[i] = new NameFinderME(model);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Try out Named entity recognition with the pre-trained models.
     * Hint: preforms really badly on Indian name, the organization
     * recognizer also performs pretty poorly.
     *
     * @param fileName Name of the file with sentences that act as input to
     *                 the recognizer. 
     *
     * @return void
     */
    public void runExample(String fileName) {
        try(Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach((sentence) -> {
                    System.out.println(sentence);
                    String[] tokenizedSentence = te.tokenize(sentence);

                    System.out.println("Persons ....");
                    Span[] pSpans = nameFinders[0].find(tokenizedSentence);
                    for(Span span : pSpans) {
                        System.out.println(span);
                    }

                    System.out.println("Organizations ....");
                    Span[] oSpans = nameFinders[1].find(tokenizedSentence);
                    for(Span span : oSpans) {
                        System.out.println(span);
                    }

                    System.out.println();
                });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
