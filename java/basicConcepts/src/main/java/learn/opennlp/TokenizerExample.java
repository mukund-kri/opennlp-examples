package learn.opennlp;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;

import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/**
 * Shows how to do tokenization with opennlp.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.15
 */
class TokenizerExample {

    TokenizerME tokenizer;

    /**
     * Load the pre-trained, maximum entropy tokenizer model
     */
    TokenizerExample() {
        try(InputStream modelFileStream = new FileInputStream("en-token.bin")){
            TokenizerModel model = new TokenizerModel(modelFileStream);
            tokenizer = new TokenizerME(model);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tokenize the input string using the tokenizer model loaded in the
     * constructor.
     *
     * @param inputSent The sentence to be tokenized as String
     *
     * @return String[] String array of tokens
     */
    public String[] tokenize(String inputSent) {
        return this.tokenizer.tokenize(inputSent);
    }


    /**
     * Tokenize the input string and print the tokens out. Serves as an
     * basic example on OpenNLP tokenization
     *
     * @param inputSent The sentence to tokenize as String
     *
     * @return void
     */
    public void runExample(String inputSent) {
        System.out.println("\n\nTOKENIZER EXAMPLE\n");
        String[] tokens = this.tokenize(inputSent);
        for(String token : tokens) {
            System.out.println(token);
        } 
    }
}
