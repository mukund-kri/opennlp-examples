package learn.opennlp;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.util.Sequence;


class POSTaggerExample {

    POSTaggerME posTagger;
    TokenizerExample te = new TokenizerExample();

    /**
     * Load the pre-trained, maximum entropy Part of Speech tagger.
     */
    POSTaggerExample() {
        try(InputStream modelFile = new FileInputStream("en-pos-maxent.bin")) {
            POSModel model = new POSModel(modelFile);
            this.posTagger = new POSTaggerME(model);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Tokenize and do Parts of Speech tagging on a sentence. Return two
     * arrays of Strings. The first one is tokens the second is the tags for
     * tokens. Useful as an input for lemmatization.
     *
     * @params sentence The sentence to tokanize and tag. String
     *
     * @return String[][]
     */
    public String[][] tokensAndTags(String sentence) {

        String[][] returnValue = new String[2][];
        String[] tokenizedSentence = te.tokenize(sentence);
        String[] tags = posTagger.tag(tokenizedSentence);

        returnValue[0] = tokenizedSentence;
        returnValue[1] = tags;

        return returnValue;
    }

    /**
     * Run the example OpenNLP code for Part of Speech tagging.
     *
     * @params fileName String, name of the file with the sentences on which
     *                  to do POS tagging.
     *
     * @return void
     */
    public void runExample(String fileName) {
        System.out.println("POS Tagger Example\n");

        try(Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach((sentence) -> {
                    System.out.println(sentence);
                    String[] tokenizedSentence = te.tokenize(sentence);
                    String[] tags = posTagger.tag(tokenizedSentence);
                    for(int i=0; i<tags.length; i++) {
                        System.out.println
                            (tokenizedSentence[i]
                             + " :: "
                             + tags[i]);
                    }
                    System.out.println();

                    Sequence[] topSequences = posTagger.topKSequences(tokenizedSentence);
                    for(Sequence sequence : topSequences) {
                        System.out.println(sequence.getOutcomes());
                    }
                });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
